//Storage contorler
const StorageCtrl = (function() {
	//public methods
	return {
		storeItem: function(item) {
			let items;
			//See if any item in storage
			if (localStorage.getItem('items') === null) {
				items = [];
				items.push(item);
				//set local storage
				localStorage.setItem('items', JSON.stringify(items));
			} else {
				//retrive from local storage
				items = JSON.parse(localStorage.getItem('items'));

				//push
				items.push(item);
				//reset local storage
				localStorage.setItem('items', JSON.stringify(items));
			}
		},
		retriveFromStorage: function() {
			let items;
			if (localStorage.getItem('items') === null) {
				items = [];
			} else {
				items = JSON.parse(localStorage.getItem('items'));
			}
			return items;
		},
		updateItemStorage: function(updatedItem) {
			let items = JSON.parse(localStorage.getItem('items'));
			items.forEach((item, index) => {
				if (updatedItem.id === item.id) {
					items.splice(index, 1, updatedItem);
				}
			});
			localStorage.setItem('items', JSON.stringify(items));
		},
		deleteItemFromStorage: function(id) {
			let items = JSON.parse(localStorage.getItem('items'));
			items.forEach((item, index) => {
				if (id === item.id) {
					items.splice(index, 1);
				}
			});
			localStorage.setItem('items', JSON.stringify(items));
		}
	};
})();
//item controller
const ItemCtrl = (function() {
	console.log('item');
	//Construtor
	function Item(id, name, calories) {
		this.id = id;
		this.name = name;
		this.calories = calories;
	}
	//State
	const state = {
		// items: [
		// 	// {
		// 	// 	id: 0,
		// 	// 	name: 'food 0',
		// 	// 	calories: 100
		// 	// }
		// ],
		items: StorageCtrl.retriveFromStorage(),
		currentItem: null,
		totalCalories: 0
	};

	return {
		getItems: function() {
			return state.items;
		},
		addItem: function(name, calories) {
			console.log(name, calories);
			let ID;
			//create id
			state.items.length > 0 ? (ID = state.items[state.items.length - 1].id + 1) : (ID = 0);
			//Calories to number
			calories = parseInt(calories);
			//Create new item
			newItem = new Item(ID, name, calories);
			state.items.push(newItem);

			return newItem;
		},
		getTotalCalories: function() {
			let total = 0;
			state.items.forEach((item) => {
				total += item.calories;
			});
			//Set total cal in state structure
			state.totalCalories = total;
			return state.totalCalories;
		},
		logState: function() {
			return state;
		},
		getItemById: function(id) {
			let found = null;
			state.items.forEach((item) => {
				if (item.id === id) {
					found = item;
				}
			});
			return found;
		},
		updateItem: function(name, calories) {
			//turn cal to numbers
			calories = parseInt(calories);

			let found = null;
			state.items.forEach((item) => {
				if (item.id === state.currentItem.id) {
					item.name = name;
					item.calories = calories;
					found = item;
				}
			});
			return found;
		},
		setCurrentItem: function(item) {
			state.currentItem = item;
		},
		getCurrentItem: function() {
			return state.currentItem;
		},
		deleteItem: function(id) {
			//get ids
			const ids = state.items.map((item) => {
				return item.id;
			});
			//index
			const index = ids.indexOf(id);
			//remove
			state.items.splice(index, 1);
		},
		removeAllItems: function() {
			state.items = [];
		}
	};
})();

//UI controller
const UICtrl = (function() {
	const UISelectors = {
		itemList: '#item-list',
		listItems: '#item-list li',
		addBTN: '.add-btn',
		backBTN: '.back-btn',
		updateBTN: '.update-btn',
		deleteBTN: '.delete-btn',
		backBTN: '.back-btn',
		clearBTN: '.clear-btn',
		itemName: '#item-name',
		itemCalories: '#item-calories',
		totalCalories: '.total-calories'
	};
	console.log('UI ');
	return {
		fillItemList: function(item) {
			let html = '';
			item.forEach((items) => {
				html += `<li id="item-${items.id} class="collection-item">${items.name}: <em>${items.calories}</em>
                <a href="" class="secondary-content"><i class="edit-item fas fa-pencil-alt"></i></a></li>`;
			});
			document.querySelector(UISelectors.itemList).innerHTML = html;
		},
		getSelectors: function() {
			return UISelectors;
		},
		getItemInput: function() {
			return {
				name: document.querySelector(UISelectors.itemName).value,
				calories: document.querySelector(UISelectors.itemCalories).value
			};
		},
		addListItem: function(item) {
			//Show list
			document.querySelector(UISelectors.itemList).style.display = 'block';
			const li = document.createElement('li');
			li.className = 'collection-item';
			li.id = `item-${item.id}`;
			li.innerHTML = `${item.name}: <em>${item.calories}</em><a href="" class="secondary-content"><i class="edit-item fas fa-pencil-alt"></i></a>`;

			document.querySelector(UISelectors.itemList).insertAdjacentElement('beforeend', li);
		},
		updateListItem: function(item) {
			let listItems = document.querySelectorAll(UISelectors.listItems);

			//turn node list into an array
			listItems = Array.from(listItems);
			listItems.forEach(function(listItem) {
				const itemID = listItem.getAttribute('id');

				if (itemID === `item-${item.id}`) {
					document.querySelector(`#${itemID}`).innerHTML = `
					${item.name}: <em>${item.calories}</em><a href="" class="secondary-content"><i class="edit-item fas fa-pencil-alt"></i></a>
					`;
				}
			});
		},
		clearInput: function() {
			document.querySelector(UISelectors.itemName).value = '';
			document.querySelector(UISelectors.itemCalories).value = '';
		},
		addItemToFrom: function() {
			document.querySelector(UISelectors.itemName).value = ItemCtrl.getCurrentItem().name;
			document.querySelector(UISelectors.itemCalories).value = ItemCtrl.getCurrentItem().calories;
			UICtrl.showEditState();
		},
		hideList: function() {
			document.querySelector(UISelectors.itemList).style.display = 'none';
		},
		showTotal: function(total) {
			document.querySelector(UISelectors.totalCalories).textContent = total;
		},
		clearEditState: function() {
			UICtrl.clearInput();
			document.querySelector(UISelectors.updateBTN).style.display = 'none';
			document.querySelector(UISelectors.deleteBTN).style.display = 'none';
			document.querySelector(UISelectors.backBTN).style.display = 'none';
			document.querySelector(UISelectors.addBTN).style.display = 'inline';
		},
		showEditState: function() {
			document.querySelector(UISelectors.updateBTN).style.display = 'inline';
			document.querySelector(UISelectors.deleteBTN).style.display = 'inline';
			document.querySelector(UISelectors.backBTN).style.display = 'inline';
			document.querySelector(UISelectors.addBTN).style.display = 'none';
		},
		deleteListItem: function(id) {
			const itemID = `#item-${id}`;
			const item = document.querySelector(itemID);
			item.remove();
		},
		removeItems: function() {
			let listItems = document.querySelectorAll(UISelectors.listItems);

			listItems = Array.from(listItems);

			listItems.forEach((item) => {
				item.remove();
			});
		}
	};
})();

// app controller
const AppCtrl = (function(ItemCtrl, StorageCtrl, UICtrl) {
	const loadEventListners = function() {
		//Get Selectors
		const UISelectors = UICtrl.getSelectors();
		//Add item
		document.querySelector(UISelectors.addBTN).addEventListener('click', itemAddSubmit);

		//Disable submit on enter
		document.addEventListener('keypress', (evt) => {
			if (evt.keyCode === 13 || evt.which === 13) {
				evt.preventDefault();
				return false;
			}
		});

		//Edit icon click event
		document.querySelector(UISelectors.itemList).addEventListener('click', itemEditClick);

		//Update item submit
		document.querySelector(UISelectors.updateBTN).addEventListener('click', itemUpDateSubmit);

		//back button
		document.querySelector(UISelectors.backBTN).addEventListener('click', UICtrl.clearEditState);
		document.querySelector(UISelectors.deleteBTN).addEventListener('click', itemDeleteSubmit);
		//clear button
		document.querySelector(UISelectors.clearBTN).addEventListener('click', removeAllItemsClick);
	};

	//Add Item submit
	function itemAddSubmit(evt) {
		//Get form input from UICtrl
		const input = UICtrl.getItemInput();
		//Check for values
		if (input.name !== '' && input.calories !== '') {
			//add items
			const newItem = ItemCtrl.addItem(input.name, input.calories);
			//display itme
			UICtrl.addListItem(newItem);

			//Get total cals
			const totalCalories = ItemCtrl.getTotalCalories();
			//display total
			UICtrl.showTotal(totalCalories);

			//Store in local storage
			StorageCtrl.storeItem(newItem);
			//clear inout feilds
			UICtrl.clearInput();
		}

		evt.preventDefault();
	}

	//Cleck Edit item
	function itemEditClick(evt) {
		//console.log('test');
		if (evt.target.classList.contains('edit-item')) {
			//console.log('edit item');
			//get list item id
			const listId = evt.target.parentNode.parentNode.id;
			// console.log(listId);

			//break into an array the id
			const listIdArr = listId.split('-');
			// console.log(listIdArr);
			//get the id number
			const id = parseInt(listIdArr[1]);
			console.log(id);

			const itemToEdit = ItemCtrl.getItemById(id);

			//console.log(itemToEdit);
			// set current item
			ItemCtrl.setCurrentItem(itemToEdit);

			//add item back to form
			UICtrl.addItemToFrom();
		}
		evt.preventDefault();
	}

	function itemUpDateSubmit(evt) {
		//get item input
		const input = UICtrl.getItemInput();

		//update item
		const updateItem = ItemCtrl.updateItem(input.name, input.calories);
		//update ui
		UICtrl.updateListItem(updateItem);
		//Get total cals
		const totalCalories = ItemCtrl.getTotalCalories();
		//display total
		UICtrl.showTotal(totalCalories);
		//Update local storage
		StorageCtrl.updateItemStorage(updateItem);

		UICtrl.clearEditState();
		evt.preventDefault();
	}

	//delete item
	function itemDeleteSubmit(evt) {
		//get current item
		const currentItem = ItemCtrl.getCurrentItem();
		//delete
		ItemCtrl.deleteItem(currentItem.id);

		//remove from ui
		UICtrl.deleteListItem(currentItem.id);

		//Get total cals
		const totalCalories = ItemCtrl.getTotalCalories();
		//display total
		UICtrl.showTotal(totalCalories);

		//Delete
		StorageCtrl.deleteItemFromStorage(currentItem.id);

		UICtrl.clearEditState();
		evt.preventDefault();
	}
	//Remove all items
	function removeAllItemsClick(evt) {
		ItemCtrl.removeAllItems();
		UICtrl.removeItems();
		//Get total cals
		const totalCalories = ItemCtrl.getTotalCalories();
		//display total
		UICtrl.showTotal(totalCalories);

		//hide ul
		UICtrl.hideList();
		evt.preventDefault();
	}
	//Public Methods
	return {
		init: function() {
			//Set init state
			UICtrl.clearEditState();
			//Get items from state
			const items = ItemCtrl.getItems();
			if (items.length == 0) {
				UICtrl.hideList();
			} else {
				//Fill list with items
				UICtrl.fillItemList(items);
			}
			//Get total cals
			const totalCalories = ItemCtrl.getTotalCalories();
			//display total
			UICtrl.showTotal(totalCalories);
			loadEventListners();
		}
	};
})(ItemCtrl, StorageCtrl, UICtrl);

AppCtrl.init();
