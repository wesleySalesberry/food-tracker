<h1 align="center">Welcome to food-tracker 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
</p>

> Allows you to take note of the items that you have eaten during the day and allows you to guesstimate the calories. This also allows to come back later to update your calories and food item.  

### 🏠 [Homepage](https://wesleysalesberry.gitlab.io/food-tracker/)

## Author

👤 **Wesley Salesberry**


## Show your support

Give a ⭐️ if this project helped you!

***